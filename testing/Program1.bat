#!/bin/sh
fw=$1
card=$2

echo "Chose firmware $fw"
echo "Chose card $card"

if test "$fw" = "2.9" ; then 
   echo "Bad firmware" > /dev/stderr
   exit 1
fi

echo "Programming ... "
sleep 2
echo "done"

exit 0
