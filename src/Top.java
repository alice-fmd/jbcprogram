import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.text.Element;
import javax.swing.text.html.HTMLDocument;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class Top extends javax.swing.JFrame {

	{
		//Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

    /** */
    private static final long serialVersionUID = 1L;
    private AbstractAction programAction;
    private AbstractAction exitAction;
    private AbstractAction selectFMD3;
    private AbstractAction selectFMD2;
    private AbstractAction selectFMD1;
    private JMenuBar menuBar;
    private JMenuItem fileMenuProgram;
    private JMenuItem fileMenuExit;
    private JMenu fileMenu;
    private JSplitPane split;
    private JPanel content;
    private JToolBar toolBar;
    private JButton exitButton;
    private JButton programButton;
    private JPanel middle;
    private JScrollPane fwScroll;
    private JList fwList;
    private DefaultListModel fwListModel;
    private JRadioButton fmd1iU;
    private JRadioButton fmd1iD;
    private JRadioButton fmd2iU;
    private JRadioButton fmd2iD;
    private JRadioButton fmd2oU;
    private JRadioButton fmd2oD;
    private JRadioButton fmd3iU;
    private JRadioButton fmd3iD;
    private JRadioButton fmd3oU;
    private JRadioButton fmd3oD;
    private JRadioButton fmd3;
    private JRadioButton fmd2;
    private JRadioButton fmd1;
    private JScrollPane outputScroll;
    private JTextPane logView;
    private FileWriter logFile = null;
    
    // ______________________________________________________________
    // GUI Code
    /**
    * Auto-generated main method to display this JFrame
    */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                Top inst = new Top();
                inst.setLocationRelativeTo(null);
                inst.setVisible(true);
            }
        });
    }
    /** 
     * Constructor 
     */
    public Top() {
        super();
        initGUI();
        openLog();
    }
    private void openLog()
    {
        try {
            logFile = new FileWriter("bcProgram.log", true);
        }
        catch (IOException e) {
            System.err.println("Failed to open log file: " + e.getMessage());
            logFile = null;
        }
        
    }
    // ______________________________________________________________
    // Actions Code
    private AbstractAction getProgramAction() {
        if(programAction == null) {
            programAction = new AbstractAction("Program", null) {
                private static final long serialVersionUID = 1L;
                public void actionPerformed(ActionEvent evt) {
                    doProgram();
                }
            };
            programAction.putValue(javax.swing.Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("pressed P"));
            programAction.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Program selected cards with selected firmware\n\n");
        }
        return programAction;
    }
    private AbstractAction getExitAction() {
        if(exitAction == null) {
            exitAction = new AbstractAction("Exit", null) {
                private static final long serialVersionUID = 1L;
                public void actionPerformed(ActionEvent evt) {
                    System.exit(1);
                }
            };
            exitAction.putValue(javax.swing.Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("pressed X"));
        }
        return exitAction;
    }
    /**
     * Get select detector action
     * @return
     */
    private AbstractAction getSelectFMD1() {
        if(selectFMD1 == null) {
            selectFMD1 = new AbstractAction("FMD1", null) {
                private static final long serialVersionUID = 1L;
                public void actionPerformed(ActionEvent evt) {
                    fmd1iU.setSelected(fmd1.isSelected());
                    fmd1iD.setSelected(fmd1.isSelected());  
                }
            };
            selectFMD1.putValue(javax.swing.Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("pressed 1"));
        }
        return selectFMD1;
    }
    /**
     * Get select detector action
     * @return
     */
    private AbstractAction getSelectFMD2() {
        if(selectFMD2 == null) {
            selectFMD2 = new AbstractAction("FMD2", null) {
                private static final long serialVersionUID = 1L;
                public void actionPerformed(ActionEvent evt) {
                    fmd2iU.setSelected(fmd2.isSelected());
                    fmd2iD.setSelected(fmd2.isSelected());
                    fmd2oU.setSelected(fmd2.isSelected());
                    fmd2oD.setSelected(fmd2.isSelected());
                    
                }
            };
            selectFMD2.putValue(javax.swing.Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("pressed 2"));
        }
        return selectFMD2;
    }
    /**
     * Get select detector action
     * @return
     */
    private AbstractAction getSelectFMD3() {
        if(selectFMD3 == null) {
            selectFMD3 = new AbstractAction("FMD3", null) {
                private static final long serialVersionUID = 1L;
                public void actionPerformed(final ActionEvent evt) {
                    fmd3iU.setSelected(fmd3.isSelected());
                    fmd3iD.setSelected(fmd3.isSelected());
                    fmd3oU.setSelected(fmd3.isSelected());
                    fmd3oD.setSelected(true);
                }
            };
            selectFMD3.putValue(javax.swing.Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("pressed 3"));
        }
        return selectFMD3;
    }
    
    // ______________________________________________________________
    // GUI Code
    /** 
     * Initialize the GUI
     */
    private void initGUI() {
        try {
            BorderLayout thisLayout = new BorderLayout();
            getContentPane().setLayout(thisLayout);
            this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            this.setTitle("Program FMDD BC chips");
            // this.setDefaultLookAndFeelDecorated(true);
            // JFrame.setDefaultLookAndFeelDecorated(true);
            this.setJMenuBar(menuBar);
            getContentPane().add(getToolBar(), BorderLayout.NORTH);
            getContentPane().add(getSplit(), BorderLayout.CENTER);
            {
                menuBar = new JMenuBar();
                setJMenuBar(menuBar);
                {
                    fileMenu = new JMenu();
                    menuBar.add(fileMenu);
                    fileMenu.setText("File");
                    {
                        fileMenuExit = new JMenuItem();
                        fileMenu.add(getFileMenuProgram());
                        fileMenu.add(fileMenuExit);
                        fileMenuExit.setText("Exit");
                        fileMenuExit.setAction(getExitAction());
                    } 
                }
            }
            findFw();
            
            pack();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /** 
     * Create/get the menu 
     * @return
     */
    private JMenuItem getFileMenuProgram() {
        if(fileMenuProgram == null) {
            fileMenuProgram = new JMenuItem();
            fileMenuProgram.setText("Program");
            fileMenuProgram.setAction(getProgramAction());
        }
        return fileMenuProgram;
    }
    /**
     * Get the content pane
     * @return
     */
    private JPanel getContent() {
        if(content == null) {
            content = new JPanel();
            BorderLayout contentLayout = new BorderLayout();
            content.setLayout(contentLayout);
            content.setPreferredSize(new java.awt.Dimension(629, 151));
            content.add(getFwScroll(), BorderLayout.WEST);
            content.add(getMiddle(), BorderLayout.CENTER);
        }
        return content;
    }
    /** 
     * Get the split pane
     * @return
     */
    private JSplitPane getSplit() {
        if(split == null) {
            split = new JSplitPane();
            split.setPreferredSize(new java.awt.Dimension(600, 400));
            split.setOrientation(JSplitPane.VERTICAL_SPLIT);
            split.setSize(600, 500);
            split.add(getContent(), JSplitPane.TOP);
            split.add(getOutputScroll(), JSplitPane.BOTTOM);
        }
        return split;
    }
    /**
     * Creates the toolbar 
     * @return The tool bar
     */
    private JToolBar getToolBar() {
        if(toolBar == null) {
            toolBar = new JToolBar();
            toolBar.setPreferredSize(new java.awt.Dimension(597, 25));
            toolBar.add(getProgramButton());
            toolBar.add(getExitButton());
        }
        return toolBar;
    }
    /** 
     * Create the program button in the tool bar
     * @return
     */
    private JButton getProgramButton() {
        if(programButton == null) {
            programButton = new JButton();
            programButton.setText("Program");
            programButton.setAction(getProgramAction());
            programButton.setToolTipText("Program selected cards with selected firmware version");
        }
        return programButton;
    }
    
    /** 
     * Create the exit button in the tool bar
     * @return
     */
    private JButton getExitButton() {
        if(exitButton == null) {
            exitButton = new JButton();
            exitButton.setText("Exit");
            exitButton.setAction(getExitAction());
        }
        return exitButton;
    }
    /** 
     * Create/Get the firmware list 
     * @return
     */
    private JList getFwList() {
        if(fwList == null) {
            fwListModel = new DefaultListModel();
            fwList = new JList();
            fwList.setModel(fwListModel);
            // fwList.setPreferredSize(new java.awt.Dimension(108, 148));
            fwList.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
            fwList.setDragEnabled(false);
            fwList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        }
        return fwList;
    }
    /**
     * Get scroll of FW list
     * @return
     */
    private JScrollPane getFwScroll() {
        if(fwScroll == null) {
            fwScroll = new JScrollPane();
            fwScroll.setPreferredSize(new java.awt.Dimension(114, 152));
            fwScroll.setViewportView(getFwList());
        }
        return fwScroll;
    }
    /**
     * Create/Get the middle panel
     * @return
     */
    private JPanel getMiddle() {
        if(middle == null) {
            middle = new JPanel();
            GroupLayout middleLayout = new GroupLayout((JComponent)middle);
            middle.setLayout(middleLayout);
            middle.setPreferredSize(new java.awt.Dimension(300, 152));
            middleLayout.setHorizontalGroup(middleLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(middleLayout.createParallelGroup()
                    .addComponent(getFmd3(), GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd1(), GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd2(), GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(25)
                .addGroup(middleLayout.createParallelGroup()
                    .addComponent(getFmd2iU(), GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd3iU(), GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd1iU(), GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(middleLayout.createParallelGroup()
                    .addComponent(getFmd3iD(), GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd2iD(), GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd1iD(), GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(middleLayout.createParallelGroup()
                    .addComponent(getFmd3oU(), GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd2oU(), GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(middleLayout.createParallelGroup()
                    .addGroup(middleLayout.createSequentialGroup()
                        .addComponent(getFmd2oD(), GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(middleLayout.createSequentialGroup()
                        .addComponent(getFmd3oD(), GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(315, Short.MAX_VALUE));
            middleLayout.setVerticalGroup(middleLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(middleLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(getFmd1iD(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd1iU(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd1(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(middleLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(getFmd2oD(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd2oU(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd2iD(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd2iU(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd2(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(middleLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(getFmd3oD(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd3oU(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd3iD(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd3iU(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getFmd3(), GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(69, 69));
        }
        return middle;
    }
    /** 
     * Create/get FMD Board select button
     * @return
     */
    private JRadioButton getFmd1iU() {
        if(fmd1iU == null) {
            fmd1iU = new JRadioButton();
            FlowLayout fmd1iULayout = new FlowLayout();
            fmd1iU.setLayout(fmd1iULayout);
            fmd1iU.setText("FMD1iU");
            fmd1iU.setBounds(108, 96, 85, 21);
        }
        return fmd1iU;
    }
    /** 
     * Create/get FMD Board select button
     * @return
     */
    private JRadioButton getFmd1iD() {
        if(fmd1iD == null) {
            fmd1iD = new JRadioButton();
            fmd1iD.setText("FMD1iD");
            fmd1iD.setBounds(447, 30, 72, 20);
        }
        return fmd1iD;
    }
    /** 
     * Create/get FMD Board select button
     * @return
     */
    private JRadioButton getFmd2iU() {
        if(fmd2iU == null) {
            fmd2iU = new JRadioButton();
            fmd2iU.setText("FMD2iU");
            fmd2iU.setBounds(26, 5, 72, 20);
        }
        return fmd2iU;
    }
    /** 
     * Create/get FMD Board select button
     * @return
     */
    private JRadioButton getFmd2iD() {
        if(fmd2iD == null) {
            fmd2iD = new JRadioButton();
            fmd2iD.setText("FMD2iD");
            fmd2iD.setBounds(613, 5, 72, 20);
        }
        return fmd2iD;
    }
    /** 
     * Create/get FMD Board select button
     * @return
     */
    private JRadioButton getFmd2oU() {
        if(fmd2oU == null) {
            fmd2oU = new JRadioButton();
            fmd2oU.setText("FMD2oU");
            fmd2oU.setBounds(365, 30, 77, 20);
        }
        return fmd2oU;
    }
    /** 
     * Create/get FMD Board select button
     * @return
     */
    private JRadioButton getFmd2oD() {
        if(fmd2oD == null) {
            fmd2oD = new JRadioButton();
            fmd2oD.setText("FMD2oD");
            fmd2oD.setBounds(193, 30, 77, 20);
        }
        return fmd2oD;
    }
    /** 
     * Create/get FMD Board select button
     * @return
     */
    private JRadioButton getFmd3iU() {
        if(fmd3iU == null) {
            fmd3iU = new JRadioButton();
            fmd3iU.setText("FMD3iU");
            fmd3iU.setBounds(103, 5, 72, 20);
        }
        return fmd3iU;
    }
    /** 
     * Create/get FMD Board select button
     * @return
     */
    private JRadioButton getFmd3iD() {
        if(fmd3iD == null) {
            fmd3iD = new JRadioButton();
            fmd3iD.setText("FMD3iD");
            fmd3iD.setBounds(372, 5, 72, 20);
        }
        return fmd3iD;
    }
    /** 
     * Create/get FMD Board select button
     * @return
     */
    private JRadioButton getFmd3oU() {
        if(fmd3oU == null) {
            fmd3oU = new JRadioButton();
            fmd3oU.setText("FMD3oU");
            fmd3oU.setBounds(531, 5, 77, 20);
        }
        return fmd3oU;
    }
    /** 
     * Create/get FMD Board select button
     * @return
     */
    private JRadioButton getFmd3oD() {
        if(fmd3oD == null) {
            fmd3oD = new JRadioButton();
            fmd3oD.setText("FMD3oD");
            fmd3oD.setBounds(449, 5, 77, 20);
        }
        return fmd3oD;
    }
    /**
     * Get/Create FMD1 button
     * @return
     */
    private JRadioButton getFmd1() {
        if(fmd1 == null) {
            fmd1 = new JRadioButton();
            fmd1.setText("FMD1");
            fmd1.setToolTipText("Select all FMD1 cards");
            fmd1.setAction(getSelectFMD1());
            fmd1.setBounds(12, 73, 59, 20);
        }
        return fmd1;
    }
    /**
     * Get/Create FMD2 button
     * @return
     */
    private JRadioButton getFmd2() {
        if(fmd2 == null) {
            fmd2 = new JRadioButton();
            fmd2.setText("FMD2");
            fmd2.setToolTipText("Select all FMD2 cards");
            fmd2.setAction(getSelectFMD2());
            fmd2.setBounds(308, 5, 59, 20);
        }
        return fmd2;
    }
    /**
     * Get/Create FMD3 button
     * @return
     */
    private JRadioButton getFmd3() {
        if(fmd3 == null) {
            fmd3 = new JRadioButton();
            fmd3.setText("FMD3");
            fmd3.setToolTipText("Select all FMD3 cards");
            fmd3.setAction(getSelectFMD3());
            fmd3.setBounds(180, 5, 59, 20);
        }
        return fmd3;
    }
    /** 
     * Get the scroll of the output
     * @return
     */
    private JScrollPane getOutputScroll() {
        if(outputScroll == null) {
            outputScroll = new JScrollPane();
            outputScroll.setPreferredSize(new java.awt.Dimension(737, 112));
            outputScroll.setFocusTraversalPolicyProvider(true);
            outputScroll.setVerifyInputWhenFocusTarget(false);
            outputScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            outputScroll.setSize(600, 300);
            outputScroll.setViewportView(getLogView());
        }
        return outputScroll;
    }
    /**
     * Get the log view
     * @return
     */
    private JTextPane getLogView() {
        if(logView == null) {
            logView = new JTextPane();
            logView.setEditable(false);
            logView.setContentType("text/html");
            logView.setText("<html>\n <head>\n <style type=\"text/css\">\n    span.e { color: red }\n   p { text-align: left;\n         font-family: monospace; }\n </style>\n</head>\n<body id=\"body\">\n<p id=\"text\">\n</p>\n</body>\n</html>\n");
            logView.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
            logView.setPreferredSize(new java.awt.Dimension(400, 300));

        }
        return logView;
    }
    
    // ______________________________________________________________
    // Misc code 
    /**
     * Find the list of firmwares in current directory.
     */
    private void findFw()
    {
        File dir = new File(".");
        File[] files = dir.listFiles();
        Arrays.sort(files);
        
        final Pattern pattern = Pattern.compile("bc_flash-([0-9]+).([0-9]+).pof");
        
        List<Integer> vers = new ArrayList<Integer>();
        for (File file : files) {
            if (file.isDirectory()) continue;
            Matcher matcher = pattern.matcher(file.getName());
            if (!matcher.matches()) continue;
            
            int major = Integer.parseInt(matcher.group(1));
            int minor = Integer.parseInt(matcher.group(2));
            vers.add((major << 8) | minor);
            // fwListModel.addElement(matcher.group(1));
        }
        Collections.sort(vers);
        for (Integer v : vers) {
            int major = v >> 8;
            int minor = v & 0xFF;
            String s = String.format("%d.%d", major, minor);
            fwListModel.addElement(s);
        }
    }
    /** 
     * Member function to create a report 
     * @param msg
     * @param isError
     */
    private void report(String msg, boolean isError)
    {
        HTMLDocument doc = (HTMLDocument)logView.getStyledDocument();
        Element      body = doc.getElement("text");
        
        try {
            doc.insertBeforeEnd(body,"<span " + (isError ? "class=\"e\">" : ">") + msg + "</span><br>\n");
        }
        catch (Exception e) {
            System.err.println("When making a report: " + e.getMessage());
        }
        logView.repaint();
        logView.revalidate();
        
        if (logFile == null) return;
        
        DateFormat f = DateFormat.getDateTimeInstance();
        String logMsg = f.format(new Date()) + (isError ? " ERROR: " : ": ") + msg + "\n";
        try {
            logFile.write(logMsg);
            logFile.flush();
        }
        catch (IOException e) {
            System.err.println("Failed to write to log file: " + e.getMessage());
        }
    }   
    
    /** 
     * Member function to create a report 
     * @param msg
     * @param isError
     */
    private void clearLog()
    {
        HTMLDocument doc = (HTMLDocument)logView.getStyledDocument();
        Element      body = doc.getElement("text");
        
        try {
            doc.setInnerHTML(body, "");
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            System.err.println("While clearing log: " + e.getMessage());
        }
        logView.repaint();
        logView.revalidate();
    }
    // ______________________________________________________________
    // Programming code 
    //$hide>>$
    /** 
     * A class to read the output of the child command 
     * 
     * @author cholm
     *
     */
    class StreamReader implements Runnable
    {
        /**
         * Constructor
         * @param stdout
         * @param stderr
         */
        public StreamReader(InputStream stdout, InputStream stderr)
        {
            this.stdout = stdout;
            this.stderr = stderr;
        }
        /**
         * Run it 
         */
        public void run()
        {
            // TODO Auto-generated method stub
            String out = "";
            String err = "";
            BufferedReader outReader = new BufferedReader(new InputStreamReader(stdout));
            BufferedReader errReader = new BufferedReader(new InputStreamReader(stderr));
            
            try {
                while ((out = outReader.readLine()) != null ||
                       (err = errReader.readLine()) != null) 
                {
                    if (out != null && !out.isEmpty()) report(out, false);
                    if (err != null && !err.isEmpty()) {
                        report(err, true);
                        error = err;
                        err   = null;
                    }
                }
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                throw new IllegalArgumentException( "IOException receiving data from child process " + e.getMessage());
            }
        }
        /**
         * Get last error message - if any 
         * @return
         */
        public String getLastError() { return error; }
        protected InputStream stdout;
        protected InputStream stderr;
        protected String      error;
    }
    /**
     * Class to program all specified boards.  This is run as a thread. 
     * @author cholm
     *
     */
    protected class ProgramThem implements Runnable
    {
        /** 
         * Constructor
         * @param fw
         * @param cards
         * @param c
         */
        public ProgramThem(String fw, List<String> cards, Component c)
        {
            this.fw        = fw;
            this.cards     = cards;
            this.component = c;
        }
        /**
         * Program a single board 
         * @param card
         * @return
         */
        private boolean programOne(String card)
        {
            boolean result = false;
            String  error  = "";
            try {
                Process      p = Runtime.getRuntime().exec("./Program1.bat " + fw + " " + card);
                StreamReader r = new StreamReader(p.getInputStream(), p.getErrorStream());
                Thread       t = new Thread(r);
                t.start();
                p.waitFor();
                result = p.exitValue() == 0;
                t.join();
                error  = r.getLastError();
                // if (error != null && !error.isEmpty()) result = false;
            }
            catch (Exception e) {
                error  = e.getMessage();
                result = false;
            }
            if (!result) 
                JOptionPane.showMessageDialog(component, error, "Error", 
                        JOptionPane.ERROR_MESSAGE);
            return result;
        }
        /**
         * Run this thread
         */
        public void run()
        {
            clearLog();
            getProgramAction().setEnabled(false);
            getExitAction().setEnabled(false);
            
            boolean ret = true;
            for (String c : cards) {
                if (programOne(c)) {
                    continue;
                }
                report("Failed to program " + c + " with " + fw, true);
                ret = false;
                break;
            }
            if (ret)
                JOptionPane.showMessageDialog(component, "All selected cards programmed", "Info",
                        JOptionPane.INFORMATION_MESSAGE);
            report("All selected cards programmed", false);
            getProgramAction().setEnabled(true);                  
            getExitAction().setEnabled(true);                  
        }
        protected List<String> cards = null;
        protected String       fw    = null;
        protected Component    component = null;
    }
    /**
     * Member function called to program selected boards 
     */
    private void doProgram()
    {
        List<String> cards = new ArrayList<String>();
        JRadioButton[] cardSelect = { fmd1iU, fmd1iD, 
                                      fmd2iU, fmd2iD, fmd2oU, fmd2oD,
                                      fmd3iU, fmd3iD, fmd3oU, fmd3oD };
        
        String msg = "Will program ";
        for (JRadioButton b : cardSelect) {
            if (!b.isSelected()) continue;
            cards.add(b.getText());
            msg += b.getText() + " ";
        }
        if (cards.size() <= 0) {
            report("No cards selected", true);
            return;
        }
        Object fwSel = fwList.getSelectedValue();
        if (fwSel == null) {
            report("No FW selected", true);
            return;
        }
        report(msg, false);
        
        new Thread(new ProgramThem(fwSel.toString(), cards, this)).start();

    }
    //$hide<<$
    
    

}
