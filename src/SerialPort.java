/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cholm
 */
public class SerialPort
{
    enum BaudRate {
        baud110,
        baud150,
        baud300,
        baud600,
        baud1200,
        baud2400,
        baud4800,
        baud9600,
        baud19200;
        public int asInteger()
        {
            switch (this) {
            case baud110:       return   110;
            case baud150:       return   150;
            case baud300:       return   300;
            case baud600:       return   600;
            case baud1200:      return  1200;
            case baud2400:      return  2400;
            case baud4800:      return  4800;
            case baud9600:      return  9600;
            case baud19200:     return 19200;
            }
            return 0;
        }
    }
    enum Parity {
        none,
        even,
        odd,
        mark,
        space;
    }
    enum WordSize {
        bit5,
        bit6,
        bit7,
        bit8;
        public int asInteger() 
        {
            switch (this) {
            case bit5: return 5;
            case bit6: return 6;
            case bit7: return 7;
            case bit8: return 8;
            }
            return 0;
        }
    }
    enum StopBits {
        one,
        three_halfs,
        two;
        public float asFloat() 
        {
            switch (this) {
            case one:         return 1;
            case three_halfs: return 1.5f;
            case two:         return 2;
            }
            return 0;
        }
    }
    enum TimeOut {
        on,
        off
    }
    enum XonHandshake {
        on,
        off
    }
    enum DataSetReady {
        on,
        off
    }
    enum ClearToSend {
        on,
        off
    }
    enum DataTerminalReady {
        on,
        off,
        handshake;
        public String toString() {
            switch(this) {
            case on:        return "on";
            case off:       return "off";
            case handshake: return "hs";
            }
            return "";
        }
    }
    enum RequestToSend {
        on,
        off,
        handshake,
        toggle;
        public String toString() {
            switch(this) {
            case on:        return "on";
            case off:       return "off";
            case handshake: return "hs";
            case toggle:    return "tg";
            }
            return "";
        }
    }
    enum DataSetReadySensitive {
        on,
        off
    }
    
    /**
     * constructor 
     * @param which Which port to open
     */
    public SerialPort(String which)
    {
        _port_name = which;
    }

    public boolean configure(BaudRate              baud, 
                             Parity                parity, 
                             WordSize              size,
                             StopBits              stop, 
                             TimeOut               timeout, 
                             XonHandshake          xon,
                             DataSetReady          dsr, 
                             ClearToSend           cts,
                             DataTerminalReady     dtr, 
                             RequestToSend         rts,
                             DataSetReadySensitive dsrs)
    {

        
        String cmd[] = { "cmd.exe", "/c", "start", "/min", "mode.com",
                _port_name, 
                "baud=" + baud.asInteger(), 
                "parity=" + parity.toString(),
                "data=" + size.asInteger(), 
                "stop=" + stop.asFloat(), 
                "to=" + timeout.toString(),
                "xon=" + xon.toString(), 
                "odsr=" + dsr.toString(),
                "octs=" + cts.toString(), 
                "dtr" + dtr.toString(), 
                "rts=" + rts.toString(),
                "idsr=" + dsrs };
        try {
            Process p = Runtime.getRuntime().exec(cmd);

            if (p.waitFor() != 0) {
                System.err.println("We cannot go on");
            }
        }
        catch (Exception e) {
            System.err.println("@while configuring port: " + e.getMessage());
            return false;
        }
        return true;
    }
    /** 
     * Open the port 
     * @return {@code true} on success. 
     */
    public boolean open() {
        try {
            close();
            _port = new RandomAccessFile(_port_name,"rws");

        } catch (IOException ex) {
            Logger.getLogger(SerialPort.class.getName()).log(Level.SEVERE, null, ex);
            _port = null;
            return false;
        }
        return true;
    }
    /**
     * Close the port 
     */
    public void close() {
        try {
            if (_port != null) {
                _port.close();
            }
            _port = null;
        } catch (IOException ex) {
            Logger.getLogger(SerialPort.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Write a string to the port 
     * @param s String to write 
     * @return {@code true} on success, {@code false} otherwise. 
     */
    public boolean write(String s)
    {
        if (_port == null) {
            Logger.getLogger(SerialPort.class.getName())
                .log(Level.SEVERE, "No port opened");
            return false;
        }
        try {
            _port.writeBytes(s);
        } catch (IOException ex) {
            Logger.getLogger(SerialPort.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    /**
     * Read a string from the serial port.  Up to {@code delim} is read. 
     * @param timeout Time-out for read in milliseconds 
     * @param delim   What to read up to.
     * @return Read string, or null in case of problems
     */
    public String read(int timeout, int delim) {
        if ( _port == null) {
            Logger.getLogger(SerialPort.class.getName())
                .log(Level.SEVERE, "No reader opened");
            return null;
        }
        String s ="";
        int   c = 0;
        try {
            do {
                c = _port.read();
                if (c == delim || c == 0) break;
                s += (char)c;
            } while (c != delim && c != 0);
        } catch (Exception ex) {
            Logger.getLogger(SerialPort.class.getName())
                .log(Level.SEVERE, "Failed in read at " + s, ex);
            return null;
        }
        return s;
    }
    /** 
     * Skip to end of stream
     */
    public void skipToEnd()
    {
        try {
            _port.seek(_port.length());
        } catch (IOException ex) {
            Logger.getLogger(SerialPort.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Read a line from the serial port. 
     * @param timeout Time-out in milliseconds
     * @return Read line, or null in case of problems
     */
    public String read(int timeout)
    {
        return read(timeout, (int)'\n');
    }
    /**
     * Read a line from the serial port. 
     * @return Read line, or null in case of problems
     */
    public String read()
    {
        return read(1, (int)'\n');
    }
    /** Name of the port */
    protected String _port_name = "";
    /** The port */
    protected RandomAccessFile _port = null;
}
