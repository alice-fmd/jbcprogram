/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *
 * @author cholm
 */
public class JTagSelector
{
    /**
     * Constructor
     */
    public JTagSelector()
    {
        _port = new SerialPort("COM1");
        if (!(_is_open = _port.open())) {
            System.err.println("Failed to open port");
            return;
        }
    }
    /**
     * Check if the port is opened
     */
    public boolean isOpen() { return _is_open; }
    /**
     * Execute a command
     * @param cmd Command to execute
     * @return String with result, or null in case of problemms
     */
    protected String executeCmd(String cmd)
    {
        if (!_is_open) return null;
        String cmd1 = cmd + "\n";
        if (!_port.write(cmd1)) {
            System.err.printf("Failed to execute \"%s\"", cmd1);
            return null;
        }
        String ret = "";
        // Check for errors
        if ((ret = _port.read(100)) == null || ret.contains("ERROR:")) {
            System.err.println("Failed to read result: " + ret);
            ret = null;
            // Read the second error line
            _port.read(100);
        }
        // System.out.printf("Result of command is \"%s\"\n", ret);
        return ret;
    }
    protected String findSubStr(String r, String pat)
    {
        if (r == null) return null;
        Pattern pattern = Pattern.compile(pat);
        Matcher matcher = pattern.matcher(r);
        if (!matcher.lookingAt()) {
            System.err.printf("Failed to match \"%s\" to \"%s\"\n", r, pat);
            return null;
        }
        return matcher.group(1);
    }
    protected String findBoard(String r)
    {
       if (r == null) return r;
       return findSubStr(r,"JTAG=\\S+ \\(BOARD=(\\S+)\\)");
    }
    protected int findSlot(String r)
    {
        if (r == null) return -1;
        String ret = findSubStr(r, "JTAG=(\\S+) \\(BOARD=\\S+\\)");
        int slot = -1;
        if (ret != null && !ret.equalsIgnoreCase("NONE"))
            slot = Integer.parseInt(ret);
        return slot;
    }
    protected boolean findButton(String r)
    {
        if (r == null) return false;
        String ret = findSubStr(r, "Button (\\S+)");
        if (ret == null) return false;
        return ret.equalsIgnoreCase("enabled");
    }
    public String getBoard()
    {
        return findBoard(executeCmd("QUERY_SLOT"));
    }
    public int getSlot()
    {
        return findSlot(executeCmd("QUERY_SLOT"));
    }
    public boolean setBoard(String board)
    {
      String b = findBoard(executeCmd("BOARD=" + board));
      if (!board.equals(b)) {
          System.err.printf("Failed to set board to %s, got %s\n", board, b);
          return false;
      }
      return true;
    }
    public boolean setSlot(int slot)
    {
        if (slot < 0) slot = -1;
        String s = (slot < 0 ? "NONE" : String.valueOf(slot));
        int    t = findSlot(executeCmd("JTAG=" + s));
        if (t != slot) {
            System.err.printf("Failed to set slot to %d, got %d\n", slot, t);
            return false;
        }
        return true;
    }
    public boolean getButton()
    {
        return findButton(executeCmd("QUERY_BUTTON"));
    }
    public boolean setButton(boolean on)
    {
        boolean o = findButton(executeCmd((on ? "ENABLE" : "DISABLE") + "_BUTTON"));
        if (o != on) {
            System.err.println("Faield to set button state");
            return false;
        }
        return true;
    }
    protected boolean _is_open = false;
    protected SerialPort _port = null;
}
